<?php

abstract class Table {

    public $amount = 0;
    public $limit = 50;
    public $max_visible_pages = 10;
    public $direction;
    public $order_by;
    public $search;
    public $current_page;
    
    public function __construct() {
        $this->limit = (int) $this->getValue($_REQUEST, 'limit', $this->limit);
        $this->order_by = $this->getRequest('order_by');

        $this->direction = $this->getValue($_REQUEST, 'direction', 'ASC');
        if (!in_array(strtolower($this->direction), array('asc', 'desc'))) {
            $this->direction = '';
        }
        $current_page = (int) $this->getValue($_REQUEST, 'current_page', 1);
        $first_page = $this->getRequest('first_page');
        if ($first_page == 1) {
            $current_page = 1;
        }
        $this->current_page = $current_page;
    }

    public static function getRequest($name_param, $default_value = '') {
        return isset($_REQUEST[$name_param]) ? $_REQUEST[$name_param] : $default_value;
    }

    public static function getValue($arr, $name_param, $default_value = '') {
        return isset($arr[$name_param]) ? $arr[$name_param] : $default_value;
    }

    public function getFields() {
        return array();
    }

    public function processingField($data_row, $field) {
        return $data_row[$field];
    }

    public function getSortableColumns() {
        return array();
    }

    abstract public function getData();

    public function processingRow($data_row) {
        return '<tr>';
    }

    public function getDataTable() {
        $data = $this->getData();

        $current_page = $this->getCurrentPage();
        $amount_pages = (int) ($this->amount / $this->limit);
        if ($amount_pages < ($this->amount / $this->limit)) {
            $amount_pages+=1;
        }
        $start_page = $current_page - ((int) ( $this->max_visible_pages / 2 ));
        $end_page = $current_page + ((int) ( $this->max_visible_pages / 2 ));

        if ($end_page > $amount_pages) {
            $end_page = $amount_pages;
        }
        if ($start_page < 1) {
            $start_page = 1;
        }
        $fields = $this->getFields();
        $start_el = ($current_page - 1) * $this->limit + 1;
        $end_el = ($start_el - 1) + $this->limit;
        if ($end_el > $this->amount) {
            $end_el = $this->amount;
        }

        $data = is_array($data) ? $data : array();

        $table_data = array(
            'Table' => $this,
            'data' => $data, 'fields' => $fields,
            'pagination' => array('limit' => $this->limit, 'current_page' => $current_page, 'amount_pages' => $amount_pages, 'start_page' => $start_page, 'end_page' => $end_page),
            'order_by' => $this->order_by,
            'direction' => $this->direction,
            'start_el' => number_format($start_el),
            'end_el' => number_format($end_el),
            'total' => number_format($this->amount),
            'sortables' => $this->getSortableColumns()
        );
        return $table_data;
    }

    public function getCurrentPage() {
        return $this->current_page;
    }

}
