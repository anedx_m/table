<?php

class ExampleTable extends Table {

    public function processingField($data_row, $field) {
        switch ($field) {
            case 'link':
                return '<a href="' . $data_row[$field] . '" target=_blank>' . $data_row[$field] . '</a>';
            case 'actions':
                return '<input type="button" data-id="' . $data_row['id'] . '" class="btn btn-default edit" href="#" value="Edit">';
            default:
                return $data_row[$field];
        }
    }

    public function getSortableColumns() {
        return array('name');
    }

    public function processingRow($data_row) {
        return '<tr class="row" data-id="' . $data_row['id'] . '">';
    }

    public function getFields() {
        return array(
            'name' => 'Name',
            'link' => 'Link',
            'actions' => ''
        );
    }

    public function display() {
        $this->limit = 3;
        $data_table = $this->getDataTable();
        $this->view('template-table', array('data_table' => $data_table));
    }

    private function view($view, $params = array()) {
        extract($params);

        $path_table_view = __DIR__ . "/../views/$view.php";

        include($path_table_view);
    }

    public function getData() {

        $data = array(
            1 => array('id' => 1, 'name' => 'test1', 'link' => 'http://test1.com'),
            2 => array('id' => 2, 'name' => 'test2', 'link' => 'http://test2.com'),
            3 => array('id' => 3, 'name' => 'test3', 'link' => 'http://test3.com'),
            4 => array('id' => 4, 'name' => 'test4', 'link' => 'http://test4.com'),
            5 => array('id' => 5, 'name' => 'test5', 'link' => 'http://test5.com'),
            6 => array('id' => 6, 'name' => 'test6', 'link' => 'http://test6.com'),
            7 => array('id' => 7, 'name' => 'test7', 'link' => 'http://test7.com'),
        );
        //-=-=-=-
        $this->amount = count($data);
        //-=-=-=-
        $current_page = $this->getCurrentPage();
        $offset = ($current_page - 1) * $this->limit;
        return array_slice($data, $offset, $this->limit);

    }

}
