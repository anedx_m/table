<?php

class ExampleTable2 extends Table {

    public function processingField($data_row, $field) {
        switch ($field) {
            case 'link':
                return '<a href="' . $data_row[$field] . '" target=_blank>' . $data_row[$field] . '</a>';
            case 'actions':
                return '<input type="button" data-id="' . $data_row['id'] . '" class="btn btn-default edit" href="#" value="Edit">';
            default:
                return $data_row[$field];
        }
    }

    public function getSortableColumns() {
        return array('name','link');
    }

    public function processingRow($data_row) {
        return '<tr class="row" data-id="' . $data_row['id'] . '">';
    }

    public function getFields() {
        return array(
            'name' => 'Name',
            'link' => 'Link',
            'actions' => ''
        );
    }

    public function display() {
        $this->limit = 3;
        $data_table = $this->getDataTable();
        $this->view('template-table', array('data_table' => $data_table));
    }

    private function view($view, $params = array()) {
        extract($params);

        $path_table_view = __DIR__ . "/../views/$view.php";

        include($path_table_view);
    }

    public function getData() {
        $db = new PDO('mysql:host=localhost;dbname=simple-table;', 'root', '1');
        
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

        $current_page = $this->getCurrentPage();

        $table = 'test';

        $where = '';

        $search = $this->getValue($_REQUEST, 'search');
        $prepare_params = array();
        if ($search) {
            $term = $this->getValue($search, 'term');
            $where = "WHERE concat(name,' ',link) LIKE :search";
            $prepare_params[':search'] = "%$term%";
        }
        $order_by = $this->order_by;
        $order_by_sql = '';
        $direction = $this->direction;

        $sc = $this->getSortableColumns();
        if ($order_by AND in_array($order_by, $sc)) {
            $order_by_sql = "ORDER BY $order_by $direction";
        }

        $limit = $this->limit;
        $limit_sql = '';
        if ($limit) {
            $offset = ($current_page - 1) * $limit;
            $limit_sql = "LIMIT $offset,$limit";
        }
        $sql = "SELECT * FROM $table $where $order_by_sql $limit_sql";
        
        $data = $db->prepare($sql);
        $r = $data->execute($prepare_params);
        $data = $data->fetchAll(PDO::FETCH_NAMED);

        $amount = $db->prepare("SELECT COUNT(*) FROM $table " . $where);

        $r = $amount->execute($prepare_params);
        $amount = $amount->fetch(PDO::FETCH_COLUMN);
        //-=-=-=-=-=-=-=-=-
        $this->amount = $amount;
        //-=-=-=-=-=-=-=-=-
        return $data;
    }

}
