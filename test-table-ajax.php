<html>
    <head>
        <link rel="stylesheet" href="css/table.css">
        <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="js/table.js"></script>
    </head>
    <body>
        <form id="form-search">
            <input name="term" id="term">
            <input type="button" id="btn-search" value="search">
        </form>
        <div id="table"></div>
        <script>
            var target = '#table';
            
            var table = $(target).table('get-table.php');
            table.list();
            table.afterList.add(function () {
                table.find('.edit').click(function () {
                    alert('Edit');
                })
            })
            $('#term').keydown(function (e) {
                if (event.which == 13) {
                    event.preventDefault();
                    table.list({search: {term: $('#term').val()}}, 1);
                }
            })
            $('#btn-search').click(function () {
                table.list({search: {term: $('#term').val()}}, 1);
            })
        </script>
    </body>
</html>

