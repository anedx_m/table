(function ($) {

    $.fn.table = function (url) {
        var that = this
        this.url = url;
        this.params = {};

        this.afterList = $.Callbacks();

        this.list = function (params, first_page) {

            if ($.type(params) !== 'object') {
                params = {}
            }

            if (typeof (params.order_by) == 'undefined') {
                params.order_by = $(that).find('.order_by').val();
            }
            if (typeof (params.direction) == 'undefined') {
                params.direction = $(that).find('.direction').val();
            }

            var current_page = $(that).find('.current-page').attr('data-page');

            if (first_page == 1) {
                params.first_page = 1
            } else {
                params.first_page = 0
            }
            that.params = params;
            $.post(this.url, params, function (data) {
                $(that).html(data)
                that.params.first_page = 0;
                that.afterList.fire()
            }, 'html')

        }

        $(this).on('click', 'th.sortable', function () {
            var order_by = $(this).attr('data-order_by')
            var direction = $(that).find('.direction').val();

            if (direction == 'ASC') {
                direction = 'DESC'
            } else {
                direction = 'ASC'
            }
            $(that).find('.order_by').val(order_by)
            $(that).find('.direction').val(direction)
            delete that.params.order_by;
            delete that.params.direction;
            that.list(that.params, 1)
        })

        $(this).on('click', '.number-page', function (e) {
            e.preventDefault();
            var page = $(this).attr('data-page');
            that.params.current_page = page;

            $.post(that.url, that.params, function (data) {
                $(that).html(data)
            })
        })
        return this;
    }
})(jQuery);