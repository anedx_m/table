<input type="hidden" value="<?php echo $data_table['order_by'] ?>" class="order_by">
<input type="hidden" value="<?php echo $data_table['direction'] ?>" class="direction">

<?php
if ($data_table['pagination']['amount_pages'] > 0):
    ?>
    <div class="pages_info">
        <span><?php echo $data_table['start_el'] ?> - <?php echo $data_table['end_el'] ?> of <?php echo $data_table['total'] ?></span>
    </div>
<?php endif; ?>
<table class="table table-hover table-striped">
    <tr class="table-header">
        <?php
        foreach ($data_table['fields'] as $field => $alias):
            $sortable = in_array($field, $data_table['sortables']) ? 'sortable' : '';
            ?>
            <th data-order_by="<?php echo $field ?>" class="<?php
            echo $data_table['order_by'] == $field ? $data_table['direction'] : '';
            echo " $sortable th-field-$field"  ;
            ?>"><?php echo $alias ?></th>
                <?php
            endforeach;
            ?>
    </tr>
    <?php
    foreach ($data_table['data'] as $data_row) {
        $row = $data_table['Table']->processingRow($data_row);
        echo $row;
        foreach ($data_table['fields'] as $field => $alias):
            ?>
            <td class="<?php echo "field-$field" ?>"><?php
                echo $data_table['Table']->processingField($data_row, $field);
                ?></td>
            <?php
        endforeach;
        ?>
    </tr>
    <?php
}
?>
</table>
<?php
if ($data_table['pagination']['amount_pages'] > 0):
    ?>
    <div class="pages_info">
        <span><?php echo $data_table['start_el'] ?> - <?php echo $data_table['end_el'] ?> of <?php echo $data_table['total'] ?></span>
    </div>
    <div class="t-pagination">
        <?php
        if ($data_table['pagination']['start_page'] > 1):
            ?>
            <a class="prev number-page" data-page="<?php echo $data_table['pagination']['start_page'] - 1 ?>"><i class="fa fa-angle-double-left"></i></a>
            <?php
        endif;
        for ($i = $data_table['pagination']['start_page']; $i <= $data_table['pagination']['end_page']; $i++):
            if ($i == $data_table['pagination']['current_page']):
                ?>
                <a class="number-page current-page" data-page="<?php echo $i ?>"><?php echo $i ?></a>
                <?php
            else :
                ?>
                <a class="number-page" href="?current_page=<?php echo $i ?>" data-page="<?php echo $i ?>"><?php echo $i ?></a>
            <?php
            endif;
        endfor;
        if ($data_table['pagination']['end_page'] < $data_table['pagination']['amount_pages']):
            ?>
            <a class="next number-page" data-page="<?php echo $data_table['pagination']['end_page'] + 1 ?>"><i class="fa fa-angle-double-right"></i></a>
            <?php
        endif;
        ?>
    </div>
    <div class="clearfix"></div>
    <?php
endif;
?>
